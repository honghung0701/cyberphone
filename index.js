<!doctype html>
<html lang="en">
  <head>
    <title>Title</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">\
    <link
      rel="stylesheet"
      href="https://cdn.jsdelivr.net/npm/tailwindcss@2.1.2/dist/tailwind.min.css"
    />
    <link rel="stylesheet" href="./css/index.css">
  </head>
  <body>
      <header class=" fixed w-full bg-white">
        <div class="container mx-auto h-20">
            <p class="logo text-2xl">Cyberphone</p>
            <nav>
                <a href="#">Home</a>
                <a href="#">About</a>
                <a href="#">Shop</a>
                <a href="#">Blog</a>
                <a href="#">Contact</a>
            </nav>
        </div>
      </header>
      <div class="banner h-screen pt-20 ">
        <div class="container flex mx-auto h-full">
          <div class="infor w-1/2 flex flex-col justify-center space-y-10 h-full">
            <h2 class="font-bold text-5xl">Cyberphone</h2>
            <h3 class="font-medium text-xl">Best Smartphone of 2021!</h3>
            <p class="text-base">Cras tortor mi, lobortis quis ornare in, varius scelerisque urna. Vivamus imperdiet dolor nec odio convallis consequat.</p>
            <div>
            <button class="btn-infor">More Infor</button>
            <button class="btn-shop">Shop</button>
          </div>
          </div>
          <div class="img w-1/2 h-full p-5">
            <img src="./image/product-header2.png" alt="" class="w-full">
          </div>
        </div>
      </div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>